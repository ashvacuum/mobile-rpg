﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public static event Action<Health> OnHealthAdded = delegate { };
    public static event Action<Health> OnHealthRemoved = delegate { };

    [SerializeField]
    private int _maxHealth = 100;

    public int currentHealth { get; private set; }

    public event Action<float> OnHealthPctChanged = delegate { };

    private void OnEnable() {
        currentHealth = _maxHealth;
        OnHealthAdded(this);
    }

    public void ModifyHealth(int amount) {
        currentHealth += amount;

        float currentHealthpct = (float)currentHealth / (float)_maxHealth;
        OnHealthPctChanged(currentHealthpct);
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            ModifyHealth(-10);
        }
    }

    private void OnDisable() {
        OnHealthRemoved(this);
    }
}
