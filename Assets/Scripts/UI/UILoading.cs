﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MRPG.UI
{
    public class UILoading : UIScreen
    {
        [Range(0, 1)]
        private float _progress;

        //happens when progress is completed 
        public static event OnProgressComplete ProgressComplete;
        public delegate void OnProgressComplete();

        public override void DisableScreen()
        {
            base.DisableScreen();
        }

        public override void EnableScreen()
        {
            base.EnableScreen();
        }

        protected override void OnScreenDisable()
        {
            base.OnScreenDisable();
            _progress = 0;
        }

        protected override void OnScreenEnable()
        {
            base.OnScreenEnable();
        }

        protected override void Awake()
        {
            base.Awake();
            _progress = 0;

        }

        public void AddProgressPercentage(float amount, float total)
        {
            float percent = amount / total;
            _progress = Mathf.Clamp01(percent);
            if(_progress == 1)
            {
                ProgressComplete?.Invoke();
            }
        }
    }
}
