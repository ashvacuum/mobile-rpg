﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MRPG.UI {
    public class UIManager : MonoBehaviour  
    {
        [SerializeField]
        private ScreenType _defaultScreen;
        [SerializeField]
        private List<UIScreen> _uiScreens;

        private void Awake(){

        }

        private void OnEnable(){
            SwitchScreen(_defaultScreen);
        }

        public void SwitchScreen(ScreenType type){
            _uiScreens.ForEach((screen) =>
            {
                screen.DisableScreen();

                if(screen.type == type)
                {
                    screen.EnableScreen();
                }
            });
        }


    }

    public enum ScreenType {
        Loading = 0,
        Game = 1,
        Settings = 2,
        Leaderboards = 3,
        Ad = 4
    }
}
