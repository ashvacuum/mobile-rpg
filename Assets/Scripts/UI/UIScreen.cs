using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MRPG.UI {
    /// <summary>
    /// Base Type for UI Screen
    /// </summary>
    /// <typeparam name="ScreenType">Screen Type Enum</typeparam>
    public abstract class UIScreen : MonoBehaviour
    {
        private ScreenType _type;
        private const int UILAYER = 5;
        private const int INVISIBLELAYER = 10;

        public ScreenType type {  get { return _type; } }

        protected virtual void OnScreenEnable(){
            this.gameObject.layer = UILAYER;
        }
        protected virtual void OnScreenDisable(){
            this.gameObject.layer = INVISIBLELAYER;
        }

        public virtual void EnableScreen()
        {
            OnScreenEnable();
        }

        public virtual void DisableScreen()
        {
            OnScreenDisable();
        }

        protected virtual void Awake(){
            
        }
    }
}
