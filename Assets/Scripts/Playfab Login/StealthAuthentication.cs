﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using System;

public class StealthAuthentication : MonoBehaviour
{
    private PlayFabAuthService _AuthService = PlayFabAuthService.Instance;

    [SerializeField]
    private GetPlayerCombinedInfoRequestParams infoRequestParams;
    

    public static event StealthLoginSuccess OnStealthLoginSuccess;
    public delegate void StealthLoginSuccess();


    private void Awake() {
        _AuthService.AuthType = Authtypes.Silent;
    }

    private void Start() {
        PlayFabAuthService.OnLoginSuccess += OnLoginSuccess;
        PlayFabAuthService.OnPlayFabError += OnPlayFaberror;

        _AuthService.InfoRequestParams = infoRequestParams;

        _AuthService.Authenticate();

    }

    private void OnLoginSuccess(PlayFab.ClientModels.LoginResult result) {

    }

    private void OnPlayFaberror(PlayFabError error) {
        switch (error.Error) {
            case PlayFabErrorCode.InvalidEmailAddress:
            case PlayFabErrorCode.InvalidPassword:
            case PlayFabErrorCode.InvalidEmailOrPassword:
                
                break;

            case PlayFabErrorCode.AccountNotFound:
                
                return;
            default:
                
                break;
        }
    }




}
