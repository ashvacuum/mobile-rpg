﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameObject playerCharacter;

    private Scene _currentlyLoadedScene;
    #region Unity Callbacks
    private void Awake()
    {
        playerCharacter = GameObject.FindGameObjectWithTag("Player");
        playerCharacter.SetActive(false);
    }


    private void OnEnable()
    {
        SceneManager.sceneUnloaded += TransitionToNextScene;
        SceneManager.sceneLoaded += SetSpawnPoint;
        
    }

    private void OnDisable()
    {
        SceneManager.sceneUnloaded -= TransitionToNextScene;
        SceneManager.sceneLoaded -= SetSpawnPoint;
    }

    #endregion

    private void BeginGame()
    {
        StartCoroutine(LoadScreenTransitions());
       
    }

    private IEnumerator LoadScreenTransitions()
    {
        yield return new WaitForSeconds(1f);
        playerCharacter.SetActive(true);
    }

    private void SetSpawnPoint(Scene scene, LoadSceneMode mode)
    {
        if (mode.Equals(LoadSceneMode.Additive))
        {
            playerCharacter.transform.position = SpawnPoint.location.position;
            BeginGame();
        }
    }

    private static void EnterLevel(int levelIndex)
    {
        AsyncOperation ops = SceneManager.LoadSceneAsync(levelIndex, LoadSceneMode.Additive);
        ops.allowSceneActivation = false;
        while (!ops.isDone)
        {
            Debug.Log($"Loading: {ops.progress}");
        }
        ops.allowSceneActivation = true;
    }

    private void TransitionToNextScene(Scene scene)
    {
        SceneManager.LoadSceneAsync(scene.buildIndex, LoadSceneMode.Additive);
    }
}
