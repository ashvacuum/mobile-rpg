﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControllerOverdrive : MonoBehaviour
{
    public float moveSpeed = 6f;
    //[RequireComponent(typeof(Rigidbody))]
    Rigidbody rb;
    Camera viewCamera;
    Vector3 velocity;

    public Rigidbody Rb { get => rb; set => rb = value; }

    private void Start()
    {
        Rb = GetComponent<Rigidbody>();
        viewCamera = Camera.main;
    }

    private void FixedUpdate()
    {
        Rb.MovePosition(Rb.position + velocity * Time.fixedDeltaTime);
    }

    private void Update()
    {
        Vector3 mousePos = viewCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, viewCamera.transform.position.y));
        transform.LookAt(mousePos + Vector3.up * transform.position.y);
        velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized * moveSpeed;

    }
}
