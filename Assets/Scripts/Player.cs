﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Player : MonoBehaviour
{
    public CharacterController chara;
    public int speed = 10;
    public int gravity = 5;
    public float jump = 3f;

    private float turnSmoothT = 0.1f;
    float turnSmoothV;


    void Start()
    {
        chara = GetComponent<CharacterController>();
    }


    void Update()
    {
        float horizontal = SimpleInput.GetAxis("Horizontal");
        float vertical = SimpleInput.GetAxis("Vertical");

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        direction.y -= gravity;

        if (direction.magnitude > gravity)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothV, turnSmoothT);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
        }
        chara.Move(direction * speed * Time.deltaTime);

        Debug.Log(direction.magnitude);
    }
}
