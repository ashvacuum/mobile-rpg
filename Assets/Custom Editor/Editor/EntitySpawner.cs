﻿using UnityEditor;
using UnityEngine;

public class EntitySpawner : EditorWindow
{
    [MenuItem("Custom Tools/Entity Spawner")]
    public static void ShowWindow()
    {
        GetWindow(typeof(EntitySpawner));
    }
}
