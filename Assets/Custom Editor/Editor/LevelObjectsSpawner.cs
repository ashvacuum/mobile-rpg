﻿using UnityEditor;
using UnityEditor.PackageManager.UI;
using UnityEngine;

public class LevelObjectsSpawner : EditorWindow
{
    [MenuItem("Custom Tools/Level Objects Spawner")]
    public static void ShowWindow()
    {
        GetWindow(typeof(EntitySpawner));
    }
}
